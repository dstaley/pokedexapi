import sqlite3
import json
import os
import helpers
from collections import OrderedDict
from progress_bar import ProgressBar
conn = sqlite3.connect('pokedex.sqlite')
conn.row_factory = sqlite3.Row
c = conn.cursor()

versions = []

c.execute("""SELECT v.id, v.identifier, v.version_group_id
FROM versions v""")
r = c.fetchall()
for version in r:
    versions.append({'version':version['identifier'],'id':version['id'], 'generation':version['version_group_id']})

c.execute("""SELECT ps.id, ps.identifier, psn.name
FROM pokemon_species ps
    INNER JOIN pokemon_species_names psn ON ( ps.id = psn.pokemon_species_id  )
WHERE psn.local_language_id = 9""")
r = c.fetchall()

progress = ProgressBar(len(r))

for pokemon in r:
    pokemon = OrderedDict([
        ('id', pokemon['id']),
        ('identifier', pokemon['identifier']),
        ('name',pokemon['name']),
        ('types',[]),
        ('stats',OrderedDict([
            ('hp',0),
            ('attack',0),
            ('defense',0),
            ('special_attack',0),
            ('special_defense',0),
            ('speed',0)
            ])),
        ('height',0),
        ('weight',0),
        ('pokedex_text',""),
        ('breeding',OrderedDict([
            ('gender_rate',1),
            ('steps_to_hatch',1),
            ('hatch_counter',1),
            ('egg_groups',[])
            ])),
        ('training',OrderedDict([
            ('base_exp',1),
            ('effort_points',OrderedDict([
                ('hp',0),
                ('attack',0),
                ('defense',0),
                ('special_attack',0),
                ('special_defense',0),
                ('speed',0)
                ])),
            ('capture_rate',1),
            ('base_happiness',0),
            ('growth_rate',"")
            ]))
        ])

    for version in versions:
        not_in_generation = False
        c.execute("""SELECT psft.flavor_text
        FROM pokemon_species ps
            INNER JOIN pokemon_species_flavor_text psft ON ( ps.id = psft.species_id  )
        WHERE psft.version_id = ? AND
            psft.language_id = 9 AND
            psft.species_id = ?""", [version['id'], pokemon['id']])
        r = c.fetchone()
        try:
            pokemon['pokedex_text'] = r['flavor_text']
        except TypeError:
            not_in_generation = True


        c.execute("""SELECT p.id, pm.level, mn.name
        FROM pokemon p
            INNER JOIN pokemon_moves pm ON ( p.id = pm.pokemon_id  )
                INNER JOIN moves m ON ( pm.move_id = m.id  )
                    INNER JOIN move_names mn ON ( m.id = mn.move_id  )
        WHERE p.id = ? AND
            pm.pokemon_move_method_id = 1 AND
            pm.version_group_id = ? AND
            mn.local_language_id = 9
            ORDER BY pm.level ASC""", [pokemon['id'], version['generation']])
        r = c.fetchall()
        # moves = {'level_up':[],'machine':[]}
        moves = OrderedDict([('level_up',[]),('machine',[])])
        for move in r:
            moves['level_up'].append({'move':move['name'],'level':move['level']})

        c.execute("""SELECT p.id, mn.name, ina.name
        FROM pokemon p
            INNER JOIN pokemon_moves pm ON ( p.id = pm.pokemon_id  )
                INNER JOIN moves m ON ( pm.move_id = m.id  )
                    INNER JOIN move_names mn ON ( m.id = mn.move_id  )
                    INNER JOIN machines m1 ON ( m.id = m1.move_id  )
                        INNER JOIN items i ON ( m1.item_id = i.id  )
                            INNER JOIN item_names ina ON ( i.id = ina.item_id  )
        WHERE p.id = ? AND
            pm.version_group_id = ? AND
            pm.pokemon_move_method_id = 4 AND
            mn.local_language_id = 9 AND
            m1.version_group_id = ? AND
            ina.local_language_id = 9""", [pokemon['id'], version['generation'], version['generation']])
        r = c.fetchall()
        for move in r:
            moves['machine'].append({'move':move[1],'machine':move[2]})
        pokemon['moves'] = moves

        c.execute("""SELECT t.identifier
        FROM pokemon_types pt
            INNER JOIN types t ON ( pt.type_id = t.id  )
        WHERE pt.pokemon_id = ?""", [pokemon['id']])
        r = c.fetchall()
        pokemon['types'] = []
        for pkmn_type in r:
            pokemon['types'].append(pkmn_type['identifier'])

        c.execute("""SELECT ps.pokemon_id, ps.base_stat, ps.effort, s.identifier
        FROM pokemon_stats ps
            INNER JOIN stats s ON ( ps.stat_id = s.id  )
        WHERE ps.pokemon_id = ?""", [pokemon['id']])
        r = c.fetchall()
        for stat in r:
            identifier = stat['identifier'].replace("-","_")
            pokemon['stats'][identifier] = stat['base_stat']
            pokemon['training']['effort_points'][identifier] = stat['effort']

        c.execute("""SELECT p.height, p.weight
        FROM pokemon p
        WHERE p.id = ?""", [pokemon['id']])
        r = c.fetchone()
        pokemon['height'] = r['height']
        pokemon['weight'] = r['weight']

        c.execute("""SELECT p.base_experience
        FROM pokemon p
        WHERE p.id = ?""", [pokemon['id']])
        r = c.fetchone()
        pokemon['training']['base_exp'] = r['base_experience']

        c.execute("""SELECT ps.base_happiness, ps.capture_rate, gr.identifier
        FROM pokemon_species ps
            INNER JOIN growth_rates gr ON ( ps.growth_rate_id = gr.id  )
        WHERE ps.id = ?""", [pokemon['id']])
        r = c.fetchone()
        pokemon['training']['capture_rate'] = r['capture_rate']
        pokemon['training']['base_happiness'] = r['base_happiness']
        pokemon['training']['growth_rate'] = r['identifier']

        c.execute("""SELECT ps.gender_rate, ps.hatch_counter
        FROM pokemon_species ps
        WHERE ps.id = ?""", [pokemon['id']])
        r = c.fetchone()
        pokemon['breeding']['gender_rate'] = r['gender_rate']
        pokemon['breeding']['hatch_counter'] = r['hatch_counter']
        pokemon['breeding']['steps_to_hatch'] = (pokemon['breeding']['hatch_counter']+1)*255

        c.execute("""SELECT peg.species_id, eg.identifier
        FROM pokemon_egg_groups peg
            INNER JOIN egg_groups eg ON ( peg.egg_group_id = eg.id  )
        WHERE peg.species_id = ?""", [pokemon['id']])
        r = c.fetchall()
        pokemon['breeding']['egg_groups'] = []
        for egg_group in r:
            pokemon['breeding']['egg_groups'].append(egg_group['identifier'])

        pokemon['evolutions'] = helpers.get_evolution_chain(version['id'],pokemon['id'])

        if not os.path.exists("json/"+version['version']):
            os.makedirs("json/"+version['version'])

        if not_in_generation==False:
            f = open("json/"+version['version']+'/'+str(pokemon['id'])+'.json', 'w+')
            f.write(json.dumps(pokemon, sort_keys=False, indent=4, separators=(',', ': ')))
            f.close
    progress.update_time(pokemon['id'])
    print progress