# JSON Pokedex Generator

## WARNING: HOLY SHIT TERRIBLE CODE AHEAD
So I wrote this over the course of a few evenings, having never worked with SQL databases. I know that it's horribly unoptimized as far as SQL statements go. I mean c'mon look at this shit:

![](http://dstaleyscreenshots.s3.amazonaws.com/jsondex.py__pokeapi_20130912_210510.png)

The important thing here isn't that I took a database I had never used (or understood the [absolutely insane schema](https://twitter.com/dstaley/status/328728289101352960/photo/1)) and made JSON representations of Pokedex entries. The important thing is that I was crazy enough to do that. Bat shit insane really.

## Usage
Given a SQLite database made from [Veekun's Pokedex Data](https://github.com/veekun/pokedex), running `python jsondex.py` with everything in the same directory will generate a `json` folder with the following structure:
```
- black
	- 1.json
	- 2.json
	- ...
- black-2
	- 1.json
	- 2.json
	- ...
- blue
	- 1.json
	- 2.json
	- ...
- colosseum
	- 1.json
	- 2.json
	- ...
- crystal
	- 1.json
	- 2.json
	- ...
- ...
```

With the individual JSON files having the following structure:
```
{
    "id": 1,
    "identifier": "bulbasaur",
    "name": "Bulbasaur",
    "types": [
        "grass",
        "poison"
    ],
    "stats": {
        "hp": 45,
        "attack": 49,
        "defense": 49,
        "special_attack": 65,
        "special_defense": 65,
        "speed": 45
    },
    "height": 7,
    "weight": 69,
    "pokedex_text": "For some time after its birth, it\ngrows by gaining nourishment from\nthe seed on its back.",
    "breeding": {
        "gender_rate": 1,
        "steps_to_hatch": 5355,
        "hatch_counter": 20,
        "egg_groups": [
            "monster",
            "plant"
        ]
    },
    "training": {
        "base_exp": 64,
        "effort_points": {
            "hp": 0,
            "attack": 0,
            "defense": 0,
            "special_attack": 1,
            "special_defense": 0,
            "speed": 0
        },
        "capture_rate": 45,
        "base_happiness": 70,
        "growth_rate": "medium-slow"
    },
    "moves": {
        "level_up": [
            {
                "move": "Tackle",
                "level": 1
            },
            {
                "move": "Growl",
                "level": 3
            },
            {
                "move": "Leech Seed",
                "level": 7
            },
            {
                "move": "Vine Whip",
                "level": 9
            },
            {
                "move": "PoisonPowder",
                "level": 13
            },
            {
                "move": "Sleep Powder",
                "level": 13
            },
            {
                "move": "Take Down",
                "level": 15
            },
            {
                "move": "Razor Leaf",
                "level": 19
            },
            {
                "move": "Sweet Scent",
                "level": 21
            },
            {
                "move": "Growth",
                "level": 25
            },
            {
                "move": "Double-Edge",
                "level": 27
            },
            {
                "move": "Worry Seed",
                "level": 31
            },
            {
                "move": "Synthesis",
                "level": 33
            },
            {
                "move": "Seed Bomb",
                "level": 37
            }
        ],
        "machine": [
            {
                "machine": "TM75",
                "move": "Swords Dance"
            },
            {
                "machine": "HM01",
                "move": "Cut"
            },
            {
                "machine": "HM04",
                "move": "Strength"
            },
            {
                "machine": "TM22",
                "move": "SolarBeam"
            },
            {
                "machine": "TM06",
                "move": "Toxic"
            },
            {
                "machine": "TM32",
                "move": "Double Team"
            },
            {
                "machine": "TM16",
                "move": "Light Screen"
            },
            {
                "machine": "TM70",
                "move": "Flash"
            },
            {
                "machine": "TM44",
                "move": "Rest"
            },
            {
                "machine": "TM90",
                "move": "Substitute"
            },
            {
                "machine": "TM17",
                "move": "Protect"
            },
            {
                "machine": "TM36",
                "move": "Sludge Bomb"
            },
            {
                "machine": "TM87",
                "move": "Swagger"
            },
            {
                "machine": "TM45",
                "move": "Attract"
            },
            {
                "machine": "TM27",
                "move": "Return"
            },
            {
                "machine": "TM21",
                "move": "Frustration"
            },
            {
                "machine": "TM20",
                "move": "Safeguard"
            },
            {
                "machine": "TM10",
                "move": "Hidden Power"
            },
            {
                "machine": "TM11",
                "move": "Sunny Day"
            },
            {
                "machine": "TM94",
                "move": "Rock Smash"
            },
            {
                "machine": "TM42",
                "move": "Facade"
            },
            {
                "machine": "TM53",
                "move": "Energy Ball"
            },
            {
                "machine": "TM86",
                "move": "Grass Knot"
            },
            {
                "machine": "TM09",
                "move": "Venoshock"
            },
            {
                "machine": "TM48",
                "move": "Round"
            },
            {
                "machine": "TM49",
                "move": "Echoed Voice"
            }
        ]
    },
    "evolutions": [
        {
            "id": 2,
            "identifier": "ivysaur",
            "name": "Ivysaur",
            "trigger": "level-up",
            "evolves_from": {
                "id": 1,
                "identifier": "bulbasaur",
                "name": "Bulbasaur"
            },
            "minimum_level": 16
        },
        {
            "id": 3,
            "identifier": "venusaur",
            "name": "Venusaur",
            "trigger": "level-up",
            "evolves_from": {
                "id": 2,
                "identifier": "ivysaur",
                "name": "Ivysaur"
            },
            "minimum_level": 32
        }
    ]
}
```