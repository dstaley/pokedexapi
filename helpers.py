import sqlite3
import json
import os
from collections import OrderedDict
from progress_bar import ProgressBar
conn = sqlite3.connect('pokedex.sqlite')
conn.row_factory = sqlite3.Row
c = conn.cursor()

def location_is_in_version(version, location_to_be_checked):
    c.execute("""SELECT v.version_group_id
    FROM versions v
    WHERE v.id = ?""", [version])
    r = c.fetchone()
    version_group_id = r['version_group_id']

    c.execute("""SELECT r.identifier
    FROM locations l
        INNER JOIN regions r ON ( l.region_id = r.id  )
    WHERE l.id = ?""", [location_to_be_checked])
    r = c.fetchone()
    specific_region = r['identifier']

    c.execute("""SELECT r.identifier
    FROM version_group_regions vgr
        INNER JOIN regions r ON ( vgr.region_id = r.id  )
        INNER JOIN version_groups vg ON ( vgr.version_group_id = vg.id  )
    WHERE vgr.version_group_id = ?""", [version_group_id])
    r = c.fetchall()
    regions = []
    for region in r:
        regions.append(region['identifier'])
    return specific_region in regions

def pokemon_is_in_version(version, pokemon_id_to_be_checked):
    c.execute("""SELECT psft.flavor_text
    FROM pokemon_species_flavor_text psft
    WHERE psft.species_id = ? AND
        psft.version_id = ? AND
        psft.language_id = 9""", [pokemon_id_to_be_checked,version])
    r = c.fetchall()

    if len(r)>0:
        return True

def get_item_name(item_id):
    c.execute("""SELECT itemname.name
    FROM items i
        INNER JOIN item_names itemname ON ( i.id = itemname.item_id  )
    WHERE i.id = ? AND
        itemname.local_language_id = 9""", [item_id])
    r = c.fetchone()
    return r['name']

def get_location_name(location_id):
    c.execute("""SELECT ln.name
    FROM location_names ln
    WHERE ln.local_language_id = 9 AND
        ln.location_id = ?""", [location_id])
    r = c.fetchone()
    return r['name']

def get_pokemon_name(pokemon_id):
    c.execute("""SELECT psn.name
    FROM pokemon_species_names psn
    WHERE psn.local_language_id = 9 AND
        psn.pokemon_species_id = ?""", [pokemon_id])
    r = c.fetchone()
    return r['name']

def get_pokemon_identifier(pokemon_id):
    c.execute("""SELECT ps.identifier
    FROM pokemon_species ps
    WHERE ps.id = ?""", [pokemon_id])
    r = c.fetchone()
    return r['identifier']

def get_evolutions(pokemon_id):
    # Get the Pokemon who we're trying to see evolutions for
    c.execute("""SELECT ps.identifier, psn.name
    FROM pokemon_species ps
        INNER JOIN pokemon_species_names psn ON ( ps.id = psn.pokemon_species_id  )
    WHERE ps.id = ? AND
        psn.local_language_id = 9""", [pokemon_id])
    r = c.fetchone()
    main_pokemon = {'id':pokemon_id,'identifier':r['identifier'],'name':r['name']}

    # Get all Pokemon who evolve into a specific pokemon_species_id
    c.execute("""SELECT ps.evolves_from_species_id
    FROM pokemon_species ps
    WHERE ps.id = ?""", [pokemon_id])
    r = c.fetchone()

    evolutions = []

    if r['evolves_from_species_id']:
        if who_do_i_evolve_from(r['evolves_from_species_id'])!=None:
            evolutions.append({'id':r['evolves_from_species_id'], 'evolves_from':who_do_i_evolve_from(r['evolves_from_species_id'])})
        else:
            evolutions.append({'id':r['evolves_from_species_id']})
        c.execute("""SELECT ps.evolves_from_species_id
        FROM pokemon_species ps
        WHERE ps.id = ?""", [r['evolves_from_species_id']])
        r = c.fetchone()

        if r['evolves_from_species_id']:
            if who_do_i_evolve_from(r['evolves_from_species_id'])!=None:
                evolutions.append({'id':r['evolves_from_species_id'], 'evolves_from':who_do_i_evolve_from(r['evolves_from_species_id'])})
            else:
                evolutions.append({'id':r['evolves_from_species_id']})

    staging = {}
    staging['id'] = main_pokemon['id']
    if who_do_i_evolve_from(main_pokemon['id'])!=None:
        staging['evolves_from'] = who_do_i_evolve_from(main_pokemon['id'])

    evolutions.append(staging)

    # Get all Pokemon who evolve from a specific pokemon_species_id
    c.execute("""SELECT ps.id, ps.identifier
    FROM pokemon_species ps
    WHERE ps.evolves_from_species_id = ?""", [main_pokemon['id']])
    r = c.fetchall()

    if len(r)>0:
        for pokemon in r:
            evolutions.append({'id':pokemon['id'],'evolves_from':main_pokemon['id']})
            stage = {'id':pokemon['id'],'evolves_from':main_pokemon['id']}
            # Get all Pokemon who evolve from a specific pokemon_species_id
            c.execute("""SELECT ps.id, ps.identifier
            FROM pokemon_species ps
            WHERE ps.evolves_from_species_id = ?""", [pokemon['id']])
            stage = pokemon
            r = c.fetchall()

            if len(r)>0:
                for pokemon in r:
                    row = OrderedDict([('id',pokemon['id']),('evolves_from',stage['id'])])
                    evolutions.append(row)

    return evolutions

def has_preevolutions(pokemon_id):
    c.execute("""SELECT ps.evolves_from_species_id
    FROM pokemon_species ps
    WHERE ps.id = ?""", [pokemon_id])
    r = c.fetchone()

    if len(r)>0:
        return True
    else:
        return False

def who_do_i_evolve_from(pokemon_id):
    c.execute("""SELECT ps.evolves_from_species_id
    FROM pokemon_species ps
    WHERE ps.id = ?""", [pokemon_id])
    r = c.fetchone()

    return r['evolves_from_species_id']

def get_move_name(move_id):
    c.execute("""SELECT mn.name
    FROM moves m
        INNER JOIN move_names mn ON ( m.id = mn.move_id  )
    WHERE m.id = ? AND
        mn.local_language_id = 9""", [move_id])
    r = c.fetchone()

    return r['name']

def get_gender(gender_id):
    c.execute("""SELECT g.identifier
    FROM genders g
    WHERE g.id = ?""", [gender_id])
    r = c.fetchone()

    return r['identifier']

def get_evolution_chain(version_id,pokemon_id):
    # Get the Pokemon who we're trying to see evolutions for
    c.execute("""SELECT ps.identifier, psn.name
    FROM pokemon_species ps
        INNER JOIN pokemon_species_names psn ON ( ps.id = psn.pokemon_species_id  )
    WHERE ps.id = ? AND
        psn.local_language_id = 9""", [pokemon_id])
    r = c.fetchone()
    main_pokemon = {'id':pokemon_id,'identifier':r['identifier'],'name':r['name']}

    # # Get all Pokemon who evolve from a specific pokemon_species_id
    # c.execute("""SELECT ps.id, ps.identifier
    # FROM pokemon_species ps
    # WHERE ps.evolves_from_species_id = ?""", [main_pokemon['id']])
    # r = c.fetchall()

    evolutions = get_evolutions(pokemon_id)

    pokemon_evolutions = []

    for pkmn_evolution in evolutions:
        # Get the evolution information for a specific Pokemon evolution
        c.execute("""SELECT pe.gender_id, pe.held_item_id, pe.known_move_id, pe.location_id, pe.minimum_beauty, pe.minimum_happiness, pe.minimum_level, pe.party_species_id, pe.relative_physical_stats, pe.time_of_day, pe.trade_species_id, pe.trigger_item_id, ps.identifier as 'pokemon_identifier', et.identifier as 'evolution_trigger_identifier', ps.id
        FROM pokemon_evolution pe
            INNER JOIN pokemon_species ps ON ( pe.evolved_species_id = ps.id  )
            INNER JOIN evolution_triggers et ON ( pe.evolution_trigger_id = et.id  )
        WHERE pe.evolved_species_id = ?""", [pkmn_evolution['id']])
        r = c.fetchall()

        for e in r:
            version_safe = True
            # First, we need to make sure that the given Pokemon is in the given version's Pokedex.
            if pokemon_is_in_version(version_id, e['id']):
                pass
            else:
                version_safe = False
            if e['location_id']:
                if location_is_in_version(version_id, e['location_id']):
                    pass
                else:
                    version_safe = False

            # If the given Pokemon is contained within the version's Pokedex, it's safe to proceed.
            if version_safe==True:
                evolution = OrderedDict([
                    ('id',e['id']),
                    ('identifier',e['pokemon_identifier']),
                    ('name',get_pokemon_name(e['id'])),
                    ('trigger',e['evolution_trigger_identifier']),
                    ('evolves_from',OrderedDict([
                        ('id',pkmn_evolution['evolves_from']),
                        ('identifier',get_pokemon_identifier(pkmn_evolution['evolves_from'])),
                        ('name',get_pokemon_name(pkmn_evolution['evolves_from']))
                        ]))
                    ])
                if e['evolution_trigger_identifier']=='use-item':
                    evolution['trigger_item'] = get_item_name(e['trigger_item_id'])
                if e['minimum_happiness']:
                    evolution['minimum_happiness'] = e['minimum_happiness']
                if e['minimum_beauty']:
                    evolution['minimum_beauty'] = e['minimum_beauty']
                if e['minimum_level']:
                    evolution['minimum_level'] = e['minimum_level']
                if e['time_of_day']:
                    evolution['time_of_day'] = e['time_of_day']
                if e['location_id']:
                    evolution['location'] = get_location_name(e['location_id'])
                if e['held_item_id']:
                    evolution['held_item'] = get_item_name(e['held_item_id'])
                if e['known_move_id']:
                    evolution['known_move'] = get_move_name(e['known_move_id'])
                if e['party_species_id']:
                    evolution['pokemon_in_party'] = OrderedDict([('id',e['party_species_id']),('identifier',get_pokemon_identifier(e['party_species_id'])),('name',get_pokemon_name(e['party_species_id']))])
                if e['gender_id']:
                    evolution['gender'] = get_gender(e['gender_id'])
                if e['relative_physical_stats']:
                    evolution['relative_physical_stats'] = e['relative_physical_stats']
                pokemon_evolutions.append(evolution)

    return pokemon_evolutions